# Desarrollo e Implementación de la Interfaz de **eMeter**

[TOC]

A continuación se presenta la documentación del desarrollo de la interfaz gráfica del medidor Inteligente **eMeter**.

## Descripción General

La interfaz gráfica del **eMeter** esta desarrollada para ser desplegada en un navegador WEB, permitiendo la visualización en cualquier dispositivo computador, tableta o móvil, sin tener ningún tipo de problemas de compatibilidad con cualquier sistema operativo.  

Teniendo en cuenta el desarrollo previo del proyecto, se desarrollo la interfaz gráfica con la tarjeta de desarrollo **stamp_eMeter**, la cual esta compuesta por: 

* IMX233: Procesador fabricado por *Freescale*, arquitectura **ARM 926**, teniendo la capacidad de correr Linux-Embebido. Para este caso se uso una distribución a la medida realizada con *Buildroot 2015*.

* STM32F4: Micro-controlador fabricado por *STMicroelectronics*, basado en la arquitectura **ARM-Cortex M4**, corriendo el sistema operativo en tiempo real *ChibiOS 2.4*. 

* Conexión WiFi: Teniendo en cuenta que la interfaz se desplegara por medio de una conexión WiFi, se uso una tarjeta de red con el chip-set **RTL8192CU** de *Realtek*.

A continuación se muestra el diagrama de bloques general del **eMeter**. 

![block_g.png](https://bitbucket.org/repo/Mey8re/images/397634119-block_g.png)

##Arquitectura Software General

La arquitectura propuesta para el desarrollo de la interfaz de **eMeter**, tiene como principal característica abolir la necesidad de instalar algún software para poder desplegar las medidas adquiridas, además de ofrecer compatibilidad con cualquier sistema operativo. El diagrama de cajas de esta arquitectura se presenta a continuación: 

![block_sw_1.png](https://bitbucket.org/repo/Mey8re/images/4154339113-block_sw_1.png)

A partir de la conexión como punto de acceso usando las aplicaciones *hostapd* y *dhcpd*, se configuro un servidor básico HTTP para alojar la pagina WEB y realizar una conexión en tiempo real con el cliente usando *nodeJS*, el cual toma los datos de los archivos *JSON* generados por la aplicación *main_meter*.  La programación y depuración del chip STM32 se realizó por medio de *openocd*.

El desarrollo del software para realizar la interfaz de **eMeter** esta conformado por:

* [Construcción y configuración Linux Emebebido](Construcción y configuración Linux Emebebido)
* [Configuración Punto de acceso **WiFi**](Configuración Punto de acceso WiFi)
* [Diseño y desarrollo del Sistema de Información](Diseño y desarrollo del Sistema de Información)
* [Instalación y copia del Sistema de Información eMeter](Instalación y copia del Sistema de Información eMeter)





