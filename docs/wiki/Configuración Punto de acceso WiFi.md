#Configuración Punto de acceso **WiFi**

[TOC]

## Habilitación USB-HUB

Partiendo que la tarjeta de desarrollo tiene un USB-HUB, se realizó un programa para habilitar su funcionamiento. este programa se encuentra disponible en *web_smart/develop/imx_software/Enable_USBHOST/en_usbhost.c *. Concluido el proceso de cross-compilación, se debe copiar el ejecutable *en_usbhost* al sistema de archivos del embebido, en la ruta */usr/sbin*. 

La ejecución de este programa en el sistema embebido, es sencilla, en la consola de depuración se ejecuta:

```
[~]# en_usbhost
Begin USB Hub Reset
Reset Done.
USB Mode Change.
```
Cuando se complete este programa, el sistema detecta la red WiFi y sube el soporte que se le configuro en *kernel* anteriormente. Completado este proceso la red esta lista para ser usada. 


##Cross-Compilación **hostapd**

Hostapd es un demonio en espacio de usuario para crear puntos de acceso y servidores de autenticación. Este programa implementa el estándar IEEE 802.11 para puntos de acceso, el IEEE 802.1X/WPA/WPA2/EAP de autenticación.

Debido al uso de la Llave Wifi RTL8192CU se debe usar una versión de **hostapd** especial que ofrece el fabricante de la llave, esta versión esta especialmente modificada para que funcione con este dispositivo. La versión esta disponible en las descargas del repositorio con el nombre **OJO SUBIR ARCHIVO**:

```
#!
https://bitbucket.org/kpatino/web_smart/downloads
```

Luego de realizar la descarga y descomprimir el archivo, se debe revisar el Makefile y se debe realizar el proceso de cross-compilación con el siguiente comando:
```
#!
[hostapd_rtl8192cu] make CC=arm-linux-gcc
```
Al finalizar este proceso se crea un archivo ejecutable llamado *hostapd* y *hostapd_cli*, este ejecutable se debe copiar en la carpeta */usr/sbin* y */usr/bin*  respectivamente en el sistema de archivos del embebido. 

## Configuración en el Sistema de archivos

Teniendo la imagen del kernel y el sistema de archivos andando se debe verificar que nombre usa el sistema para identificar la llave WiFi. Para esto se ejecuta:

```

[~]# iwconfig                                                                      
wlan0   IEEE 802.11b  ESSID:"IfLab"  Nickname:"<WIFI@REALTEK>"        
Mode:Master  Frequency:2.412 GHz  Access Point: 44:33:4C:61:C3:18     
Bit Rate:11 Mb/s   Sensitivity:0/0                                    
Retry:off   RTS thr:off   Fragment thr:off                            
Encryption key:off                                                    
Power Management:off                                                  
Link Quality:0  Signal level:0  Noise level:0                         
Rx invalid nwid:0  Rx invalid crypt:0  Rx invalid frag:0              
Tx excessive retries:0  Invalid misc:0   Missed beacon:0     
```

Ahora es necesario subir la la interfaz WiFi y asignarle una dirección IP, para esto se ejecuta en la consola:

```

[~]# ifconfig wlan0 192.168.50.1 up 
```

###Configuración **hostapd**

El archivo de configuración para ejecutar la aplicación **hostadp** se creó en la carpeta */etc/* del sistema embebido. Este archivo se debe establecer las configuraciones del punto de acceso. El archivo creado contiene la siguiente información.
```

[etc]# more hostapd.conf
interface=wlan0                                                                
driver=rtl871xdrv                                                               
ssid=eMeter                                                            
channel=1                                                                       
bridge=0 
```

En este archivo se especifica la interfaz wifi que se va usar. Se le debe especificar cual driver debe usar para implementar el punto de acceso para esta llave WiFi es *rtl871xdrv*. En este archivo también se define el nombre de la red y el canal donde se va ubicar la red. Para correr la aplicación con esta configuración se ejecuta:

```

[~]# hostapd /etc/hostapd.conf &
```

Al terminar la ejecución ya esta creado el demonio del *hostpot*, ya la red esta creado, sin embargo la conexión se debe realizar manual pues aún no se ha configurado el servidor *DHCP*, para la asignación automática de *IP.

###Configuración Servidor DHCP

En una red de comunicaciones es necesario asignar direciones IP a la hora de establecer una conexión, *ISC-DHCP* es una aplicación que ofrece la asignación de estas direcciones de forma fácil y rápida. Este es un programa "open source" que implementa un protocolo de configuración dinámica en el *host* para conectar a un una red IP.

Primero se debe crear un archivo donde indique cual de las interfaces de red usara por defecto. Para esto en el sistema embebido se ejecuta:

```

[etc]# mkdir default  //Crear carpeta
[default]# more isc-dhcp-server //Muestra el contenido del archivo
INTERFACES="wlan0" 
```
Luego en la carpeta */etc/dhcp* se debe crear un archivo llamado *dhcpd.conf*. Este archivo contiene:

```
[dhcp] # more dhcpd.conf
subnet 192.168.50.0 netmask 255.255.255.0 {
range 192.168.50.5 192.168.50.20;
option domain-name-servers 192.168.50.1, 192.168.50.1;option routers 192.168.50.1;
}
```

Teniendo la configuración lista para correr el servidor *DHCP* se debe ejecutar:

```
[~] # touch /var/lib/dhcp/dhcpd.leases &
[~] # dhcpd -cf /etc/dhcp/dhcpd.conf wlan0 &
```

En este momento ya esta corriendo el servidor *DHCP* y la conexión a la red inalámbrica se realiza de forma automática.

###Inicio automático

En este momento ya esta configurado tanto el punto de acceso a la red como el servidor de direcciones IP, solo queda configurar el sistema para que inicie automáticamente y no se tenga que ejecutar de nuevo las instrucciones descritas anteriormente. Para esto se debe crear un nuevo archivo en */etc/init.d*.

El archivo debe tener:

```
[~] # more /etc/init.d/S81hostapd 
en_usbhost &
sleep 10
ifconfig wlan0 192.168.50.1 &
hostapd /etc/hostapd.conf &
touch /var/lib/dhcp/dhcpd.leases &
dhcpd -cf /etc/dhcp/dhcpd.conf wlan0 &
```

Ademas se le deben dar permiso de ejecución a este archivo, para esto se ejecuta:

```
[~] # chmod +x /etc/init.d/S81hostapd 
```

De esta forma queda configurada el punto de acceso en el eMeter.