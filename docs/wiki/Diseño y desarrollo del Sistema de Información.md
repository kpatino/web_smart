#Diseño y desarrollo del Sistema de Información

[TOC]

Completadas todas las configuraciones, para el funcionamiento como punto de acceso de la plataforma, se presenta el desarrollo del sistema de información en tiempo real de **eMeter**.

#Arquitectura Software

La interfaz de **eMeter** fue desarrollada para ser desplegada en un navegador WEB, independiente del sistema operativo del dispositivo donde se ejecute la interfaz. A continuación se presenta la arquitectura Software del desarrollo WEB de la interfaz:

![block_sw_2.png](https://bitbucket.org/repo/Mey8re/images/827434977-block_sw_2.png)

##Escritura de Archivos JSON

Los algoritmos de medida se están realizando en el **STM32**, comunicando los resultados por medio de un puerto serial entre los procesadores. Estos resultados son leídos y plasmados en archivos JSON. El proceso de lectura y escritura del archivo JSON en el IMX233 se realizo por medio del programa **meter_main**. Este programa se encuentra en el directorio: *web_smart/develop/imx_software/measurement*.

En esta aplicación se escriben 2 archivos, *consumption.json* con los valores de medida de tensión, corriente, potencias y energía; y *harmonics.json*  con los valores de los armónicos y el THD de cada una de la fases. Estos archivos JSON se guardan en el directorio: */root/node_eMeter/* del sistema de archivos. A continuación se muestra parte del código donde se especifica la ruta y el nombre de los archivos:

```
#!C
139 static FILE *file;
140 file = fopen("/root/node_eMeter/consumption.json","w"); 
```

```
#!C

187 static FILE *file_harmonics;
188 file_harmonics = fopen("/root/node_eMeter/harmonics.json","w"); 
```

Luego de realizar el proceso de CROSS_COMPILACIÓN, se debe copiar el ejecutable **meter_main** en la carpeta */usr/sbin* del sistema de archivos de la tarjeta de desarrollo.


## NodeJS

Teniendo listo el programa que escribe los valores de las medidas en los archivos *harmonics.json* y *consumption.json*, se usa **nodeJS** para realizar la conexión en tiempo real entre el servidor y el cliente para poder transmitir los datos y así visualizarlos en la interfaz WEB desarrollada.

En la siguiente Figura se muestra el diagrama de bloques del desarrollo e **nodeJS**:

![block_sw_3.png](https://bitbucket.org/repo/Mey8re/images/2378909626-block_sw_3.png)

Se utilizaron los módulos **Express** y **Socket.IO**, la instalación de estos módulos en el sistema de archivos se mostró [Construcción y configuración Linux Emebebido](Construcción y configuración Linux Emebebido). El desarrollo de la aplicación de **nodeJS** se encuentra en el archivo: *web_smart/develop/web/node_eMeter/index.js*

**Express** es un framework de **nodeJS** robusto y flexible para crear aplicaciones web. Se uso para establecer la base de la aplicación, además de crear el servidor HTTP en puerto **3700**. A continuación se muestra el segmento de código que establece el framework y el servidor HTTP:

```
#!javascript

2  /* Express Framework set-up */
3  var express = require("express");
4  var http = require('http');
5  var fs     = require('fs');
6  var app = express();
7
8  app.set('port', process.env.PORT || 3700);
9  app.set('ip', process.env.IP || '192.168.50.1');
10 var server = http.createServer(app).listen(app.get('port'), app.get('ip') ,function(){
11  console.log('Server running on: %s:%d', app.get('ip') , app.get('port'));
12 });
```

**Socket.IO** es un módulo de **nodeJS** para establecer una comunicación bidireccional entre el servidor y el cliente en tiempo real.  A continuación se muestra la forma de iniciar y establecer la conexión servidor-cliente:


```
#!javascript

15 /* Socket.IO set-up */
16 var io = require('socket.io').listen(server);

25 /* Socket.IO Connection */
26 
27 io.sockets.on('connection', function(socket) {   
28   
29   socket.on('page_render', function(data){
30     page_render = parseInt(data.data);
31   });
32  
33   function update() {                                                             
34     readJson();
35     socket.emit('data', {data:data , date:date_json , harm:harm}); // Emit-event send Data to Client side                 
36     setTimeout(update, updateInterval);                                           
37   };
38  
39   update();
40                                                            
41 });
```

Teniendo en cuenta que los datos de las medidas se encuentran alojados en los archivos *consumption.json* y *harmonics.json*. Para su lectura y envío por el Socket se leyó la información con la siguiente función:

```
#!javascript

23 var updateInterval = 2500; // Read File Time

43 function readJson(){                                                     
44    var fs = require('fs');
45    data = fs.readFileSync('/root/node_eMeter/consumption.json','utf8');
46    harm = fs.readFileSync('/root/node_eMeter/harmonics.json','utf8');
47    var d = new Date();
48    date_json = d.getTime();                                                         
49 }  

```

Para la instalación de este desarrollo en **eMeter** se debe copiar la carpeta *web_smart/develop/web/node_eMeter/* al sistema de archivos de la tarjeta de desarrollo en la carpeta */root/*. 

## Aplicación WEB - Cliente

La aplicación WEB se desarrolló usando HTML5, Javascript y CSS. Logrando una interfaz gráfica independiente del sistema operativo y de la necesidad de instalar software en el dispositivo de visualización. 

La arquitectura software de la pagina WEB se presenta en la siguiente Figura:

![block_sw_4.png](https://bitbucket.org/repo/Mey8re/images/370982401-block_sw_4.png)

El desarrollo se dividió en dos partes, el desarrollo de la interfaz(forma,colores,etc) se implemento usando código en HTML y el motor de las gráficas e interacción de la pagina se desarrolló en Javascript.

###Socket.IO

Para lograr la conexión en tiempo real con el servidor en la tarjeta de desarrollo se añadió el soporte del cliente de **Socket.IO** del modulo de **nodeJS**. Para esto se añadió al encabezado de cada uno de los archivos HTML el siguiente código:

```
#!html

 <script src="src/socket.io.js"></script>
```

Además de establecer en cada uno de los archivos motores de Javascript el siguiente código:

```
#!javascript

var socket = io.connect('http://192.168.50.1:3700');
```
Como se puede ver, se establece la conexión al puerto 3700 donde esta corriendo el servidor HTML de nodeJS, así de esta forma se tiene la conexión en tiempo real entre el servidor y el cliente.

###Bootstrap - JQuery

Se uso este framework y plantillas basadas en HTML5, Javascript y CSS, el cual permite realizar y establecer el entorno gráfico general de la aplicación WEB. El soporte y la documentación del API de este complemento se puede observar en el siguiente link: [Bootstrap API](http://getbootstrap.com/getting-started/).

En cada una de los archivos HTML se agregó el siguiente encabezado para establecer el uso de este complemento en la pagina WEB:

```
#!html

 <link href="src/css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <script src="src/js/jquery-1.11.2.min.js"></script>
 <script src="src/js/bootstrap.min.js"></script>
```

###FlotCharts

Es un complemento para la realización de gráficas en JavaScript, el API de este complemento se encuentra disponible en el siguiente link: [FlotCharts API](https://github.com/flot/flot/blob/master/API.md). Para agregar este complemento a la pagina WEB se tiene que agregó el siguiente encabezado en las paginas WEB:

```
#!html
<script language="javascript" type="text/javascript" src="src/js/flot/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="src/js/flot/jquery.flot.min.js"></script>
<script language="javascript" type="text/javascript" src="src/js/flot/jquery.flot.resize.min.js"></script>
<script language="javascript" type="text/javascript" src="src/js/flot/jquery.flot.tooltip.min.js"></script>
<script language="javascript" type="text/javascript" src="src/js/flot/jquery.flot.pie.min.js"></script>
<script language="javascript" type="text/javascript" src="src/js/flot/jquery.flot.time.min.js"></script>
```

Este complemento necesita soporte de JQuery, sin embargo cuando se agregó el encabezado de **Bootstrap** se agregó soporte para JQuery, por tanto el uso y la forma de imprimir los valores se realiza por medio de sentencias especiales de JQuery. 

### Conexión Tiempo Real Medidas - Gráficas y Tablas

Desde el servidor se envía un evento por medio del Socket, llamado **data**, el cual envía por medio del Socket los datos leídos de los archivos JSON .Esto se puede ver en el código del servidor que se muestra a continuación:

```
#!javascript
/* node_eMeter/index.html
socket.emit('data', {data:data , date:date_json , harm:harm});     // Emit-event send Data to Client side      
```

En el cliente, luego de especificar y establecer la conexión al Socket, se describe cual es la repuesta al evento **data** generado en el servidor. La función que responde este evento se encuentra en los archivos motores de cada pagina de la siguiente manera:

```
#!javascript
/*web/eMeter/code**.js

socket.on('data', function(data_sm){
    data = JSON.parse(data_sm.data);
    console.log(data);

```

Luego de la transmisión en tiempo real de los datos, a partir del uso y aplicación de los API's de los complementos explicados se realizan las gráficas en tiempo real de las medidas obtenidas en el medidor inteligente. 