#Construcción y configuración Linux Emebebido

[TOC]

El desarrollo de la interfaz gráfica de **eMeter** se realizo a partir del uso de una distribución de Linux a la medida corriendo sobre el procesador *IMX233*. El proceso de configuración del *Kernel* y *Sistema de Archivos* se describe a continuación:

##Formato de la Tarjeta SD

Para poder crear una distribución de Linux Embebido para **eMeter** es necesario dar un formato especial a una memoria SD, pues este sera el disco duro del sistema embebido.

Primero es necesario tener una memoria microSD de al menos 2GB de capacidad. Para darle el formato se deben seguir las siguientes instrucciones.

Primero se debe conectar la memoria al computador. Para mirar como es el nombre de esta memoria se ejecuta el comando *dmesg*. Para este caso el se tomara que el computador la detecto como */dev/sdb*.

Primero se debe desmontar la memoria, para esto se ejecuta el siguiente comando

```
#!plain
[~]$ sudo umount /dev/sdb* 

```
Luego con la aplicación fdisk se le dará formato a cada una de las particiones, para esto se ejecuta:

```
#!plain
[~] $ sudo fdisk /dev/sdb 

```
Esta aplicación obre una interfaz en la consola, el primer paso para realizar en *fdisk* es borrar las particiones existentes en la memoria. Esto se hace:

```
#!plain
Command (m for help): d
Partition number (1-4): 
```

En el campo de *Partition number* se debe ingresar todos los numero de las particiones hasta que no exista ninguna, para revisar eso basta poner el *Command* la letra *p*.

Luego de tener la tabla de particiones vacía se debe crear una primera partición de *200MB*. Para esto en fdisk se realiza:

```
#!plain
Command (m for help): n               //Comando para crear una nueva particion
Partition type:
p   primary (0 primary, 0 extended, 4 free)
e   extended
Select (default p): p                 //Selecionar tipo de particion Primaria
Partition number (1-4, default 1): 1  //Nombrar la particion con el numero 1
```

Luego de esto el programa despliega desde donde es el inicio de la partición, para asignar el valor por defecto solo hay que oprimir la tecla *Enter*. Luego se le debe indicar el final de la partición para esto se debe ingresar *+200M*. Estas instrucciones en la interfaz se ve como:

```
#!plain
First sector (2048-7626751, default 2048):  //Presionar Enter
Using default value 2048
Last sector, +sectors or +size{K,M,G} (2048-7626751, default 7626751):+200M
Partition 1 of type Linux and of size 200 MiB is set
```

Ahora se debe crear la segunda partición en el espacio que queda en la microSD, para esto se realiza:

```
#!plain
Command (m for help): n               //Comando para crear una nueva particion
Partition type:
p   primary (0 primary, 0 extended, 4 free)
e   extended
Select (default p): p                 //Selecionar tipo de particion Primaria
Partition number (1-4, default 1): 2  //Nombrar la particion con el numero 2
```

Luego se fija y el final de la partición por defecto:


```
#!plain
First sector (411648-7626751, default 411648): 
Using default value 411648
Last sector, +sectors or +size{K,M,G} (411648-7626751, default 7626751): 
Using default value 7626751
Partition 2 of type Linux and of size 3.5 GiB is set
```

Ahora es necesario cambiar el tipo de partición 1 a OnTrack DM6 (53), en el programa se ejecuta:

```
#!plain
Command (m for help): 	t           // Comando para Cambiar tipo de Particion
Partition number (1-4): 1
Hex code (type L to list codes): 53  
Changed system type of partition 1 to 53 (OnTrack DM6 Aux3)
```

Se realiza el mismo procedimiento para la partición 2 pero esta se cambia a Linux (83):

```
#!plain
Command (m for help): 	t           // Comando para Cambiar tipo de Particion
Partition number (1-4): 2
Hex code (type L to list codes): 83
Changed system type of partition 1 to 83 (Linux)
```

Al final de este proceso la tabla de particiones se debe ver de la siguiente manera:

```
#!plain
Command (m for help): 	p         // Comando para ver tabla
Device Boot      Start         End      Blocks   Id  System
/dev/sdb1            2048      411647      204800   53  OnTrack DM6 Aux3
/dev/sdb2          411648     7626751     3607552   83  Linux
```

Para finalizar la creación de las particiones se deben guardar los cambios de la tabla de particiones, para esto se ejecuta:

```
#!plain
Command (m for help): w        //Comando para Guardar la tabla
The partition table has been altered!

Calling ioctl() to re-read partition table.
Syncing disks.
```

Teniendo lista las particiones de la microSD es necesario dar formato a la segunda partición, para esto se ejecuta:

```
#!plain
[~]$ sudo mkfs.ext3 /dev/sdb2
```

Teniendo listo el formato de la segunda partición la tarjeta microSD esta lista para almacenar el *b*otlader*, la imagen del kernel y el sistema de archivos para la distribución de Linux Embebido.

##Herramientas de Cross-Compilación y Sistema de Archivos

*Buildroot* es un conjunto de  *Makefiles* y parches para la generación fácil de Linux embebido. Puede generar todas las herramientas de cross-compilación, el sistema de archivos, la imagen del kernel y la imagen del  *Bootlader*. Esta herramienta tiene soporte para arquitecturas: ARM, MIPS, X86, PowerPC, entre otras. Sus características mas importantes son, la facilidad de configuración, debido las diferentes interfaces de configuración(*menuconfig*) que presenta y el soporte a cientos de paquetes para aplicaciones y librerías para ser usadas en espacio de usuario.

###Descarga y configuración

Para realizar la descarga de  *Buildroot* se puede realizar siguiendo los pasos que se muestran la pagina oficial de esta proyecto, sin embargo para **eMeter** se tiene disponible las configuraciones preliminares para su funcionamiento en la tarjeta de desarrollo. Descargue *Buildroot* para **eMeter** en el siguiente repositorio:

```
#!plain
https://bitbucket.org/kpatino/web_smart/downloads
```

Allí se descarga el archivo  *OJO SUBIR ARCHIVO*. Luego de descomprimir este archivo se debe ubicar desde  la consola en esa carpeta. Se debe establecer la configuración a compilar, para esto en la consola ejecute:

```
#!plain
[buildroot-2015]$ make menuconfig
```

Este comando abre la interfaz de configuración de  *BuildRoot*, allí se pueden realizar todas tipo configuraciones para la creación de las herramientas de cross-compilación, los paquetes y librerías que se añadirán al sistema de archivos. En esta distribución de  *BuildRoot* ya se le ha dado soporte a aplicaciones como : boa, hostapd, minicom, node, openocd, dhcp, entre otras.

No obstante es importante precisar algunas configuraciones, iniciando con la configuración del sistema, para ello es necesario ingresar a la opción de  *System Configuration* para comprobar la configuración del puerto donde se ubicara la consola de depuración, la velocidad de este puerto, además de configuraciones como el nombre del *Usuario-Host* y *contraseña*.  A continuación se muestra como se configuraron estas opciones:

```
#!plain
System configuration  --->
  (buildroot) System hostname
  (Welcome to eMeter) System banner  
    Passwords encoding (md5)  ---> 
    Init system (BusyBox)  --->    
    /dev management (Static using device table)  --->                                  
  (system/device_table.txt) Path to the permission tables                                
  (system/device_table_dev.txt) Path to the device tables                                
    Root FS skeleton (default target skeleton)  --->                                   
  ()  Root password                  
    /bin/sh (busybox' default shell)  --->                                             
  [*] Run a getty (login prompt) after boot                                              
    getty options  --->  
        (ttyAM0) TTY port                  
        Baudrate (115200)  --->        
        (vt100) TERM environment variable  
  ()  other options to pass to getty        
  [*] remount root filesystem read-write during boot                                     
  ()  Network interface to configure through DHCP                                        
  [ ] Install timezone info          
  ()  Path to the users tables       
  ()  Root filesystem overlay directories                                                
  ()  Custom scripts to run before creating filesystem images                            
  ()  Custom scripts to run after creating filesystem images 
```
En la opción  *getty options* se especifica el puerto serial donde ubica la consola, se debe verificar que la opción *TTYPort* este en *ttyAM0*, esto debido al uso de la versión de *kernel 2.6.35*, el cual apunta al puerto serial con ese nombre.

###Módulos de *nodeJS*

Además de realizar configuraciones del sistema la interfaz de configuración de *Buildroot* permite agregar o quitar aplicaciones y librerías del sistema de archivos. En este se agregaron módulos de *nodeJS* para realizar el desarrollo de la interfaz gráfica.

Para realizar esta configuración se debe ubicar en la opción de nodeJs. 

```
#!plain
Target Package ->
  Interpreter languages and scripting  ---> 
     [*]nodejs
         Module Selection  --->
             [*] NPM for the target
             [*] Express web application framework
             [ ] CoffeeScript 
            (socket.io) Additional modules 
```

Los módulos adicionales que se necesitan para *nodeJS* se deben escribir en la opción de  *Additional modules*. Para este caso se adicionó el modulo *socket.io*.

###SQLite

En el desarrollo de historiales de la interfaz gráfica de **eMeter**, se ve la necesidad de usar alguna librería para el manejo de base datos, para ello se tomaron las librerías de *SQLite*. Para realizar la configuración de estas librerías en *Buildroot*, se debe configurar como se muestra a continuación:
```
#!plain
Target packages  --->
  Libraries  --->
    Database  --->
      [*] sqlite                                                                   
        [*]   Command-line editing                                                   
        [*]   Additional query optimizations (stat3)                                 
        [*]   Enable version 3 of the full-text search engine                        
        [*]   Enable sqlite3_unlock_notify() interface                               
        [*]   Set the secure_delete pragma on by default                             
        [*]   Disable fsync 
```
Teniendo en consideración la configuraciones descritas, solo hace falta la cross-compilación de todo el sistema de archivos, para ello se ejecuta en la consola el siguiente comando:
```
#!plain
[buildroot-2015]$ make
```
Este proceso dura algunas horas, dependiendo de la conexión a Internet y la capacidad de su computador de compilar todos los paquetes. Cuando todo haya concluido en la consola se debe ver el siguiente mensaje:

```
#!plain
[buildroot-2015]$ >>>   Generating root filesystem image rootfs.tar
```

Esto indica que todo el proceso de generación de herramientas y compilación de aplicaciones ha concluido. Este proceso crea un archivo llamado  *rootfs.tar* con todo el sistema de archivos ubicado en  *buildroot-2015/output/images*.

###Herramientas de Cross-compilación

Las herramientas de Cross-Compilación se encuentran en el directorio: *buildroot-2015/output/host/usr/bin/*. A pesar, la compilación, no genera el enlace del archivo *arm-buildroot-linux-uclibcgnueabi-ld.real*. Este problema se soluciona con el siguiente comando:
```
#!plain
[buildroot-2015]$ cd output/host/usr/bin/
[bin]$ ln -s  arm-buildroot-linux-uclibcgnueabi-ld.real arm-linux-ld.real
```
Para usar estas herramientas de cross-compilación se debe agregar este directorio a la variable de entorno $ $PATH$. Esto se logra agregando la siguiente instrucción en el archivo  **.bashrc**.

```
#!plain
export PATH=$PATH:/path_buildroot/buildroot-2013.08.1/output/host/usr/bin
```
###Almacenamiento Sistema de Archivos

Teniendo listo el sistema de archivos generado por  *BuildRoot*, solo queda guardar este en la memoria *microSD*, para esto se debe ir al directorio donde se encuentra el archivo *rootfs.tar*. Para descomprimir el sistema de archivos en la tarjeta se ejecuta:

```
#!plain
[images] $ sudo  tar -jxvf rootfs.tar  -C /media/disk/ 
```

De esta forma ya se tiene almacenada el sistema de archivos en la memoria *microSD*.

##Compilación del **Kernel** y del **Imx-Bootlest**

Con las herramientas de cross-compilación listas, se puede compilar la imagen del Kernel de linux. Primero se deben descargar los archivos fuente de la versión a compilar, en este caso se uso el *Kernel 2.6.35*, debido a que tiene soporte de audio para el procesador *IMX233*. La descarga se puede realizar del pagina web de desarrollo del kernel de Linux. Sin embargo se el *Kernel* listo para ser usado en la tarjeta de desarrollo se encuentra en el siguiente repositorio:

```
https://bitbucket.org/kpatino/web_smart/downloads
```

El archivo se llama *linux-2.6.35.3.tar.bz2*, se debe descargar y descomprimir. Para cargar la configuración y empezar el proceso de cross-compilación se ejecuta en consola el siguiente comando:

```
[linux-2.6.35.3]$ make ARCH=arm CROSS_COMPILE=arm-linux- menuconfig
```
Allí se configura para que tipo de arquitectura se esta compilando, las opciones de arranque y cuales drivers de dispositivo se deben incluir en la compilación. 

###Driver de la Llave WiFi RTL8192CU

Para darle soporte a esta Llave Wifi se seleccionó las siguientes opciones:

```
Device Drivers  --->  
      [*] Networking support  --->
          [*]   Wireless LAN  --->
             <*>   Realtek 8192C USB WiFi
```

Esta llave WiFi, por defecto esta configurada para realizar métodos de ahorro de energía, sin embargo esto hace que la conexión sea lenta, aumentando los tiempos de ejecución de la aplicación, para esto se le quito el soporte a esta opción. Para esto se modifico archivo Makefile del driver:

```
[linux-2.6.35.3]$ gedit drivers/net/wireless/rtl8192cu/Makefile
```

En este archivo se debe cambiar la opción de *CONFIG\_POWER\_SAVING}:

```
Cambiar : CONFIG_POWER_SAVING = y 
por: CONFIG_POWER_SAVING = n
```

###Compilación del Kernel 2.6.35

Con la configuración completa, es hora de realizar la compilación del *Kernel*. Para esto se debe ejecutar en la consola el siguiente comando:

```
[linux-2.6.35.3]$ make ARCH=arm CROSS_COMPILE=arm-linux- 
```
Dependiendo de las especificaciones de su computador el proceso tarda unos minutos. Cuando termine este proceso se debe observar el siguiente mensaje:

```
Kernel: arch/arm/boot/zImage is ready
```

En esta linea se informa donde esta ubicado el archivo con la imagen comprimida del Kernel. 

###Compilación del **Imx-Bootlest**

Teniendo lista la imagen del Kernel es necesario crear el *Boot loader*. Para esto es necesario descargar del repositorio el archivo *Imx-bootlets-src-10.05.02.tar.bz2*. Luego de descomprimir su contenido se ubica la consola en ese directorio.

Primero fue necesario crear un enlace de *elftosb2*, para eso solo se ejecuta la siguiente instrucción:

```
[Imx-bootlets-src-10.05.02] $ sudo ln -s `pwd`/elftosb2 /usr/sbin/ 
```

Ahora se puede realizar la compilación del *Boot-loader* y el almacenamiento en la primera partición de la microSD. Para esto se ejecuta:

```
[Imx-bootlets-src-10.05.02] $ make ARCH=mx23 CROSS_COMPILE=arm-linux-
```

Al finalizar este proceso se guardo el *Boot-loader* en la primera partición y la tarjeta microSD esta lista para conectar en la tarjeta de desarrollo.

