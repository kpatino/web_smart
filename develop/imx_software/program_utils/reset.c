// file: pwm.c
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include "imx233.h"


int main(int argc, char **argv) {
int reg=0;
 
   	//reg=imx233_rd(HW_PINCTRL_MUXSEL1);
	//printf("0x%x\n",reg);
	imx233_wr(HW_PINCTRL_MUXSEL0_SET, 0x00003000);
	imx233_wr(HW_PINCTRL_DOE0_SET,  0x00000040); 
	imx233_wr(HW_PINCTRL_DOUT0_SET, 0x00000040);
	usleep(1000*1000*1);	
	imx233_wr(HW_PINCTRL_DOUT0_CLR, 0x00000040);
	usleep(1000*1000*1);
	imx233_wr(HW_PINCTRL_DOUT0_SET, 0x00000040);
		
	//reg=imx233_rd(HW_PINCTRL_MUXSEL1);
	//printf("0x%x",reg);
 
return 0; 
  
}

