/*
 *  STM32 eMeter Reader Algorithm
 *  Author: Andrés Mauricio Asprilla Valdés (amasprillav@unal.edu.co)
 *  Universidad Nacional de Colombia
 *  Based in Serial Port HART Protocol Daemon by Andrés Calderón (andres.calderon@emqbit.com)
 *  This code is licensed under the GNU General Public License V3.
 *  You can copy or modify it under the terms of that license.
 */

#include <sys/time.h>
#include <unistd.h>
#include <stdio.h>
#include <termios.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#include <iostream>
#include <fstream>
#include <stdlib.h>

#include <sstream>
#include <string>
#include <map>
 #include <cstdint>


extern "C"
{
#include <serial_helper.h>
}

#define Debug 0
#define NUM_HARMONICS 30
using namespace std;

int ttyfd = 0;

struct meter_values{ 
	float rmsV[3];
	float rmsI[4];
	float S[3];
	float P[3];
	float Q[3];
	float FP[3];
	float freq;
	float harmonics[2*30*3];
	float active_energy[3];
	float reactive_energy[3];
	float apparent_energy[3];
  float THD[2*3];
	uint32_t event[3];
	uint32_t cycles[3];
} values;

int
read_char_timeout (int fd, unsigned char &c, struct timeval &tv)
{
    static fd_set readfds, excepfds;

    FD_ZERO (&readfds);
    FD_ZERO (&excepfds);
    FD_SET (fd, &readfds);
    FD_SET (fd, &excepfds);

    if (select (fd + 1, &readfds, NULL, &excepfds, &tv) < 0)
    {
        if (Debug)
            cout << " error select" << endl;
        return 0;
    }
    if (FD_ISSET (fd, &excepfds))
        cout << "Exception on pipe" << endl;
    else if (FD_ISSET (fd, &readfds))
    {
        read (fd, &c, 1);
        if (Debug)
            cout << "[" << int (c) << "]-" << c << endl;

        return 1;
    }
    else
    {
        if (Debug)
            cout << " time out" << endl;
        return 0;
    }

    return 1;
}

void serial_read(int fd, int sz)
{
    int i;
    struct timeval tv;
    tv.tv_sec = 1;
    tv.tv_usec = 0;

    static unsigned char rbuffer[1000];

    for(i=0; i<sz; i++)
    {
        unsigned char c;
        if ( !read_char_timeout ( fd, c, tv) )
        {
            return;
        }
	#if Debug
        printf("0x%02X\n ",int(c));
	#endif        
	rbuffer[i]=c;
    }

    memcpy(&values, &rbuffer[0], sizeof(values));

}

int
main (int argc, char *argv[])
{
    int i,j;
    char id[3];
    unsigned char rbuffer[256];


    ttyfd = serial_open ("/dev/ttySP1", 115200, CLOCAL | CREAD | CS8, 1);

    printf("Energy Meter Daemon Running!\n");
    while(1) {
	tcflush (ttyfd, TCIOFLUSH);
        serial_read(ttyfd, sizeof(values));
	#if Debug
	for(i=30;i<60;i++)
		printf("%3.2f\n",values.harmonics[i]);
	
	#endif

	static FILE *file;
	file = fopen("/root/node_eMeter/consumption.json","w"); 
	fprintf(file,"%s","{");
	fprintf(file,"\"VR\": \"%3.2f\",",values.rmsV[0]);
	fprintf(file,"\"VS\": \"%3.2f\",",values.rmsV[1]);
	fprintf(file,"\"VT\": \"%3.2f\",",values.rmsV[2]);

	fprintf(file,"\"IR\": \"%3.2f\",",values.rmsI[0]);
	fprintf(file,"\"IS\": \"%3.2f\",",values.rmsI[1]);
	fprintf(file,"\"IT\": \"%3.2f\",",values.rmsI[2]);

	fprintf(file,"\"WA\": \"%3.2f\",",values.P[0]);
	fprintf(file,"\"WB\": \"%3.2f\",",values.P[1]);
	fprintf(file,"\"WC\": \"%3.2f\",",values.P[2]);

	fprintf(file,"\"VAA\": \"%3.2f\",",values.S[0]);
	fprintf(file,"\"VAB\": \"%3.2f\",",values.S[1]);
	fprintf(file,"\"VAC\": \"%3.2f\",",values.S[2]);

	fprintf(file,"\"VARA\": \"%3.2f\",",values.Q[0]);
	fprintf(file,"\"VARB\": \"%3.2f\",",values.Q[1]);
	fprintf(file,"\"VARC\": \"%3.2f\",",values.Q[2]);

	fprintf(file,"\"FPA\": \"%3.2f\",",values.FP[0]);
	fprintf(file,"\"FPB\": \"%3.2f\",",values.FP[1]);
	fprintf(file,"\"FPC\": \"%3.2f\",",values.FP[2]);

	fprintf(file,"\"KWHA\": \"%3.2f\",",values.active_energy[0]);
	fprintf(file,"\"KWHB\": \"%3.2f\",",values.active_energy[1]);
	fprintf(file,"\"KWHC\": \"%3.2f\",",values.active_energy[2]);

	fprintf(file,"\"KVAHA\": \"%3.2f\",",values.apparent_energy[0]);
	fprintf(file,"\"KVAHB\": \"%3.2f\",",values.apparent_energy[1]);
	fprintf(file,"\"KVAHC\": \"%3.2f\",",values.apparent_energy[2]);

	fprintf(file,"\"KVARHA\": \"%3.2f\",",values.reactive_energy[0]);
	fprintf(file,"\"KVARHB\": \"%3.2f\",",values.reactive_energy[1]);
	fprintf(file,"\"KVARHC\": \"%3.2f\",",values.reactive_energy[2]);

	fprintf(file,"\"EVENTA\": \"%s, ciclos: %d\",",values.event[0]==1?"SAG":values.event[0]==2?"SWEL":"No",values.cycles[0]);
	fprintf(file,"\"EVENTB\": \"%s, ciclos: %d\",",values.event[1]==1?"SAG":values.event[1]==2?"SWEL":"No",values.cycles[1]);
	fprintf(file,"\"EVENTC\": \"%s, ciclos: %d\",",values.event[2]==1?"SAG":values.event[2]==2?"SWEL":"No",values.cycles[2]);

	fprintf(file,"\"FREQ\": \"%3.2f\"",values.freq);
	fprintf(file,"%s","}");
	fclose(file); 


	static FILE *file_harmonics;
	file_harmonics = fopen("/root/node_eMeter/harmonics.json","w"); 
  
  static int i;
	static char *text_harm="\"VR\":";
	
	fprintf(file_harmonics,"%s","{");
	for(i = 0; i < 6; i++){
	
		switch(i){
			case 0:
				text_harm ="\"VR\"";
				break;
			case 1:
				text_harm ="\"VS\"";
				break;
			case 2:
				text_harm ="\"VT\"";
				break;
			case 3:
				text_harm ="\"IR\"";
				break;
			case 4:
				text_harm ="\"IS\"";
				break;
			case 5:
				text_harm ="\"IT\"";
				break;
		}
			fprintf(file_harmonics,"%s %s",text_harm,":");
			fprintf(file_harmonics,"[\n\t{ \n\t\"label\": %s %s %f %s",text_harm, ",\"THD\":", values.THD[i] ,",\"data\":[");
			for(j = 0; j < 30; j++){
				fprintf(file_harmonics,"[ %d , %f]", j+1 ,values.harmonics[30*i+j]);
				if(j<29)
				    fprintf(file_harmonics,",\n");
			}

			fprintf(file_harmonics,"%s","]\n\t}\n\t]");

			if(i<5)
				fprintf(file_harmonics,",\n");

		
		
	
	}
	fprintf(file_harmonics,"%s","}");
	fclose(file_harmonics); 
	

    }



    return 0;
}

