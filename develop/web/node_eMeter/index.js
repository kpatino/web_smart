
/* Express Framework set-up */
var express = require("express");
var http = require('http');
var fs     = require('fs');
var app = express();

app.set('port', process.env.PORT || 3700);
app.set('ip', process.env.IP || '192.168.50.1');
var server = http.createServer(app).listen(app.get('port'), app.get('ip') ,function(){
  console.log('Server running on: %s:%d', app.get('ip') , app.get('port'));
});


/* Socket.IO set-up */
var io = require('socket.io').listen(server);

/* Data var */
var page_render;                                                 
var data;
var harm;
var date_json;
var updateInterval = 2500; // Read File Time

/* Socket.IO Connection */

io.sockets.on('connection', function(socket) {   
  
  socket.on('page_render', function(data){
    page_render = parseInt(data.data);
  });
  
  function update() {                                                             
    readJson();
    socket.emit('data', {data:data , date:date_json , harm:harm});     // Emit-event send Data to Client side            
    setTimeout(update, updateInterval);                                           
  };
  
  update();
                                                            
});
                                                                                                
function readJson(){                                                     
   var fs = require('fs');
   data = fs.readFileSync('/root/node_eMeter/consumption.json','utf8');
   harm = fs.readFileSync('/root/node_eMeter/harmonics.json','utf8');
   var d = new Date();
   date_json = d.getTime();                                                         
}  


