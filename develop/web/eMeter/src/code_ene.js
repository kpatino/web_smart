
var socket = io.connect('http://192.168.50.1:3700');
var data_fs;
var data_KWHA = [];
var data_KWHB = [];
var data_KWHC = [];
var data_KVAHA = [];
var data_KVAHB = [];
var data_KVAHC = [];
var data_KVARHA = [];
var data_KVARHB = [];
var data_KVARHC = [];
var data_s1 = data_KWHA;
var date_fs;
var label_s1;

socket.on('connect', function(){
    console.log('we are conencted');
});

socket.on('reconnecting', function(){
 console.log('we are reconnecting');
});

socket.on('disconnect', function(){
    console.log('we are disconected');
});

socket.on('data', function(data){
      data_fs = JSON.parse(data.data);
      date_fs = data.date;
      addData_KWHA(data_fs);
      addData_KWHB(data_fs);
      addData_KWHC(data_fs);
      addData_KVAHA(data_fs);
      addData_KVAHB(data_fs);
      addData_KVAHC(data_fs);
      addData_KVARHA(data_fs);
      addData_KVARHB(data_fs);
      addData_KVARHC(data_fs);
      updateTables();
      console.log(date_fs)
});


// Flot Charts sample data for SB Admin template

// Flot Line Chart with Tooltips
$(document).ready(function() {
    console.log("document ready");
    //    socket.emit('page_render',{data : '2'});
    function currentFormatter(v, axis) {
        return v.toFixed(axis.tickDecimals) + " A";
    }
    
    function voltageFormatter(v, axis) {
        return v.toFixed(axis.tickDecimals) + " V";
    }
    
    var options = {
      series: {
        lines: {
            show: true
        },
        points: {
            show: true
        }
      },
      grid: {
          hoverable: true //IMPORTANT! this is needed for tooltip to work
      },
      tooltip: true,
      tooltipOpts: {
          content: "'%s' of %x is %y.4",
          shifts: {
          x: -60,
          y: 25
           }
      },
      xaxes: [ { mode: "time" } ],
		  yaxes: [ { min: 0,
		  alignTicksWithAxis: null,
					position: "left",
					tickFormatter: voltageFormatter
		              }, {
					// align if we are to the right
					alignTicksWithAxis: null,
					position: "left",
					tickFormatter: currentFormatter 
				} ],
				legend: { position: "sw" }   
    };
    
   
    function doPlot() {
    //label_s1 = "Tensión-RMS(V)";
    //label_s2 = "Corriente-RMS(A)";
			$.plot("#flot-hist_vi", [
				{ data: data_s1, label: label_s1 , color: "red" }
			//	{ data: data_s2, label: label_s2, color:"blue" ,yaxis: 2 }
			], options);
		}
		doPlot();
		
		
		var updateInterval = 100;
    function update() {
        singleUpdate(); 
        doPlot();       
        setTimeout(update, updateInterval);
    }
 
    update();
		
		
    
});

function singleUpdate() {
  console.log(parseInt($('#phase_source').val())*4+parseInt($('#ene_mode').val()));
  var menu = parseInt($('#phase_source').val())*4+parseInt($('#ene_mode').val());
  data_s1 = [];
  switch (menu) {
    case 0:
        $('h3.panel-title').replaceWith("<h3 class='panel-title'> Energía Activa Fase A </h3>");
        data_s1 = data_KWHA;
        label_s1 = 'KWH';
        break; 
    case 1:
        $('h3.panel-title').replaceWith("<h3 class='panel-title'> Energía Reactiva Fase A </h3>");
        data_s1 = data_KVARHA;
        label_s1 = 'KVARH';
        break;
    case 2:
        $('h3.panel-title').replaceWith("<h3 class='panel-title'> Energía Aparente Fase A </h3>");
        data_s1 = data_KVAHA;
        label_s1 = 'KVAH';
        break;
    case 4:
        $('h3.panel-title').replaceWith("<h3 class='panel-title'> Energía Activa Fase B </h3>");
        data_s1 = data_KWHB;
        label_s1 = 'KWH';
        break; 
    case 5:
        $('h3.panel-title').replaceWith("<h3 class='panel-title'> Energía Reactiva Fase B </h3>");
        data_s1 = data_KVARHA;
        label_s1 = 'KVARH';
        break;
    case 6:
        $('h3.panel-title').replaceWith("<h3 class='panel-title'> Energía Aparente Fase B </h3>");
        data_s1 = data_KVAHA;
        label_s1 = 'KVAH';
        break;
    case 8:
        $('h3.panel-title').replaceWith("<h3 class='panel-title'> Energía Activa Fase C </h3>");
        data_s1 = data_KWHC;
        break;
    case 9:
        $('h3.panel-title').replaceWith("<h3 class='panel-title'> Energía Reactiva Fase C </h3>");
        data_s1 = data_KWHC;
        break;
    case 10:
        $('h3.panel-title').replaceWith("<h3 class='panel-title'> Energía Aparente Fase C </h3>");
        data_s1 = data_KWHC;
        break; 
    default: 
        console.log("default")
  }
}


function addData_KWHA(data_fs) {
  data_KWHA.push([date_fs,data_fs.KWHA])
}

function addData_KWHB(data_fs) {
  data_KWHB.push([date_fs,data_fs.KWHB])    
}

function addData_KWHC(data_fs) {
  data_KWHC.push([date_fs,data_fs.KWHC])       
}

function addData_KVAHA(data_fs) {
  data_KVAHA.push([date_fs,data_fs.KVAHA])       
}

function addData_KVAHB(data_fs) {
  data_KVAHB.push([date_fs,data_fs.KVAHB])      
}

function addData_KVAHC(data_fs) {
  data_KVAHC.push([date_fs,data_fs.KVAHC])     
}


function addData_KVARHA(data_fs) {
  data_KVARHA.push([date_fs,data_fs.KVARHA])     
}

function addData_KVARHB(data_fs) {
  data_KVARHB.push([date_fs,data_fs.KVARHB])      
}

function addData_KVARHC(data_fs) {
  data_KVARHC.push([date_fs,data_fs.KVARHB])      
}



function updateTables(){

    $('td.KWHA').replaceWith("<td class='KWHA'>"+data_fs.KWHA+"</td>");
    $('td.KWHB').replaceWith("<td class='KWHB'>"+data_fs.KWHB+"</td>");
    $('td.KWHC').replaceWith("<td class='KWHC'>"+data_fs.KWHC+"</td>");
    $('td.KVAHA').replaceWith("<td class='KVAHA'>"+data_fs.KVAHA+"</td>");
    $('td.KVAHB').replaceWith("<td class='KVAHB'>"+data_fs.KVAHB+"</td>");
    $('td.KVAHC').replaceWith("<td class='KVAHC'>"+data_fs.KVAHC+"</td>");
    $('td.KVARHA').replaceWith("<td class='KVARHA'>"+data_fs.KVARHA+"</td>");
    $('td.KVARHB').replaceWith("<td class='KVARHB'>"+data_fs.KVARHB+"</td>");
    $('td.KVARHC').replaceWith("<td class='KVARHC'>"+data_fs.KVARHC+"</td>");

}




