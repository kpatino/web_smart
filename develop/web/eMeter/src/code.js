
var socket = io.connect('http://192.168.50.1:3700');
var data;

socket.on('connect', function(){
    console.log('we are conencted');
});

socket.on('reconnecting', function(){
 console.log('we are reconnecting');
});

socket.on('disconnect', function(){
    console.log('we are disconected');
});

socket.on('data', function(data_sm){
    data = JSON.parse(data_sm.data);
    console.log(data);
    updateTables();
});

// Flot Charts sample data for SB Admin template

// Flot Line Chart with Tooltips
$(document).ready(function() {
    console.log("document ready");
    socket.emit('page_render',{data : '1'});   
});

function updateTables(){

    $('td.VR').replaceWith("<td class='VR'>"+data.VR+"</td>");
    $('td.IR').replaceWith("<td class='IR'>"+data.IR+"</td>");
    $('td.VS').replaceWith("<td class='VS'>"+data.VS+"</td>");
    $('td.IS').replaceWith("<td class='IS'>"+data.IS+"</td>");
    $('td.VT').replaceWith("<td class='VT'>"+data.VT+"</td>");
    $('td.IT').replaceWith("<td class='IT'>"+data.IT+"</td>");
    $('td.WA').replaceWith("<td class='WA'>"+data.WA+"</td>");
    $('td.WB').replaceWith("<td class='WB'>"+data.WB+"</td>");
    $('td.WC').replaceWith("<td class='WC'>"+data.WC+"</td>");
    $('td.VAA').replaceWith("<td class='VAA'>"+data.VAA+"</td>");
    $('td.VAB').replaceWith("<td class='VAB'>"+data.VAB+"</td>");
    $('td.VAC').replaceWith("<td class='VAC'>"+data.VAC+"</td>");
    $('td.VARA').replaceWith("<td class='VARA'>"+data.VARA+"</td>");
    $('td.VARB').replaceWith("<td class='VARB'>"+data.VARB+"</td>");
    $('td.VARC').replaceWith("<td class='VARC'>"+data.VARC+"</td>");
    $('td.FPA').replaceWith("<td class='FPA'>"+data.FPA+"</td>");
    $('td.FPB').replaceWith("<td class='FPB'>"+data.FPB+"</td>");
    $('td.FPC').replaceWith("<td class='FPC'>"+data.FPC+"</td>");
    $('td.KWHA').replaceWith("<td class='KWHA'>"+data.KWHA+"</td>");
    $('td.KWHB').replaceWith("<td class='KWHB'>"+data.KWHB+"</td>");
    $('td.KWHC').replaceWith("<td class='KWHC'>"+data.KWHC+"</td>");
    $('td.KVAHA').replaceWith("<td class='KVAHA'>"+data.KVAHA+"</td>");
    $('td.KVAHB').replaceWith("<td class='KVAHB'>"+data.KVAHB+"</td>");
    $('td.KVAHC').replaceWith("<td class='KVAHC'>"+data.KVAHC+"</td>");
    $('td.KVARHA').replaceWith("<td class='KVARHA'>"+data.KVARHA+"</td>");
    $('td.KVARHB').replaceWith("<td class='KVARHB'>"+data.KVARHB+"</td>");
    $('td.KVARHC').replaceWith("<td class='KVARHC'>"+data.KVARHC+"</td>");   
}


