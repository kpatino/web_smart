var socket = io.connect('http://192.168.50.1:3700');
var data_fs;
var phData;
var qData=[];
var pData=[];
var sData=[];
var FP_P;
var label_s1;
var label_s2; 

socket.on('connect', function(){
    console.log('we are conencted');
});

socket.on('reconnecting', function(){
 console.log('we are reconnecting');
});

socket.on('disconnect', function(){
    console.log('we are disconected');
});

socket.on('data', function(data_sm){
      data_fs = JSON.parse(data_sm.data);
      dataUpdate();
      console.log(data_fs);
      updateTables();
});


// Flot Charts sample data for SB Admin template

// Flot Line Chart with Tooltips
$(document).ready(function() {
    console.log("document ready");
    //    socket.emit('page_render',{data : '2'});
    window.chart = new Highcharts.Chart({
        
        chart: {
            renderTo: 'flot-power',
            polar: true
        },
        
        title: {
            text: null
        },
        
        pane: {
            startAngle: 90,
            endAngle: -270
        },
    
        xAxis: {
            tickInterval: 45,
            min: 0,
            max: 360,
            labels: {
               }
        },
            
        yAxis: {
            min: 0
        },
        
        plotOptions: {
            series: {
                pointStart: 0,
                pointInterval: 45
            },
            column: {
                pointPadding: 0,
                groupPadding: 0
            }
        },
    
        series: [ {
            type: 'line',
            name: 'P',
            data: [[0],[0,10]],
            color : "red"
        },{
            type: 'line',
            name: 'S',
            data: [[0],[0,0]],
            color : "black"
        },{
            type: 'line',
            name: 'Q',
            data: [[0],[0,0]],
            color : "blue"
        }]
    });
    
   
    var updateInterval = 1000;
    function update() {
        singleUpdate();
        console.log(FP_P)
        console.log(pData)
        chart.series[0].setData([[0],pData]);
        chart.series[1].setData([[0],sData]);
        chart.series[2].setData([[0],qData]);    
        setTimeout(update, updateInterval);
    }
    
 
    update();
    
		
		
    
});


function singleUpdate() {
  //console.log("click"); 
  switch (parseInt($('#phase_source').val())) {
    case 0:
        $('h3.panel-title').replaceWith("<h3 class='panel-title'> Diagrama Fasorial de Potencia Fase A </h3>");
        break; 
    case 1:
        $('h3.panel-title').replaceWith("<h3 class='panel-title'> Diagrama Fasorial de Potencia Fase B </h3>");
        break;
    case 2:
        $('h3.panel-title').replaceWith("<h3 class='panel-title'> Diagrama Fasorial de Potencia Fase C </h3>");
        break; 
    default: 
        console.log("default")
  }
}


function updateTables(){

    $('td.WA').replaceWith("<td class='WA'>"+data_fs.WA+"</td>");
    $('td.WB').replaceWith("<td class='WB'>"+data_fs.WB+"</td>");
    $('td.WC').replaceWith("<td class='WC'>"+data_fs.WC+"</td>");
    $('td.VAA').replaceWith("<td class='VAA'>"+data_fs.VAA+"</td>");
    $('td.VAB').replaceWith("<td class='VAB'>"+data_fs.VAB+"</td>");
    $('td.VAC').replaceWith("<td class='VAC'>"+data_fs.VAC+"</td>");
    $('td.VARA').replaceWith("<td class='VARA'>"+data_fs.VARA+"</td>");
    $('td.VARB').replaceWith("<td class='VARB'>"+data_fs.VARB+"</td>");
    $('td.VARC').replaceWith("<td class='VARC'>"+data_fs.VARC+"</td>");
    $('td.FPA').replaceWith("<td class='FPA'>"+data_fs.FPA+"</td>");
    $('td.FPB').replaceWith("<td class='FPB'>"+data_fs.FPB+"</td>");
    $('td.FPC').replaceWith("<td class='FPC'>"+data_fs.FPC+"</td>");

}



function dataUpdate() {
  //console.log("click"); 
  switch (parseInt($('#phase_source').val())) {
    case 0:
          pData = [0,parseFloat(data_fs.WA)];
          FP_P = ((Math.acos(data_fs.FPA))*180)/(Math.PI);
          sData = [FP_P , parseFloat(data_fs.VAA)];
          qData = [90,parseFloat(data_fs.VARA)];
           
        break; 
    case 1:

          pData = [0,parseFloat(data_fs.WB)];
          FP_P = ((Math.acos(data_fs.FPB))*180)/(Math.PI);
          sData = [FP_P , parseFloat(data_fs.VAA)];
          qData = [90,parseFloat(data_fs.VARB)];
        break;
    case 2:
          pData = [0,parseFloat(data_fs.WB)];
          FP_P = ((Math.acos(data_fs.FPA))*180)/(Math.PI);
          sData = [FP_P , parseFloat(data_fs.VAB)];
          qData = [90,parseFloat(data_fs.VARB)];
        break; 
    default: 
        console.log("default")
  }
}

