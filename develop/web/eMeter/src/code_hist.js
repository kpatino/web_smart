
var socket = io.connect('http://192.168.50.1:3700');
var data_fs;
var data_R1 = [];
var data_S1 = [];
var data_T1 = [];
var data_R2 = [];
var data_S2 = [];
var data_T2 = [];
var data_s1 = data_R1;
var data_s2 = data_R2;
var date_fs;
var label_s1;
var label_s2; 

socket.on('connect', function(){
    console.log('we are conencted');
});

socket.on('reconnecting', function(){
 console.log('we are reconnecting');
});

socket.on('disconnect', function(){
    console.log('we are disconected');
});

socket.on('data', function(data){
      data_fs = JSON.parse(data.data);
      date_fs = data.date;
      addData_R(data_fs);
      addData_S(data_fs);
      addData_T(data_fs);
      updateTables();
      console.log(date_fs)
});


// Flot Charts sample data for SB Admin template

// Flot Line Chart with Tooltips
$(document).ready(function() {
    console.log("document ready");
    //    socket.emit('page_render',{data : '2'});
    function currentFormatter(v, axis) {
        return v.toFixed(axis.tickDecimals) + " A";
    }
    
    function voltageFormatter(v, axis) {
        return v.toFixed(axis.tickDecimals) + " V";
    }
    
    var options = {
      series: {
        lines: {
            show: true
        },
        points: {
            show: true
        }
      },
      grid: {
          hoverable: true //IMPORTANT! this is needed for tooltip to work
      },
      tooltip: true,
      tooltipOpts: {
          content: "'%s' of %x is %y.4",
          shifts: {
          x: -60,
          y: 25
           }
      },
      xaxes: [ { mode: "time" } ],
		  yaxes: [ { min: 0,
		  alignTicksWithAxis: null,
					position: "left",
					tickFormatter: voltageFormatter
		              }, {
					// align if we are to the right
					alignTicksWithAxis: null,
					position: "left",
					tickFormatter: currentFormatter 
				} ],
				legend: { position: "sw" }   
    };
    
   
    function doPlot() {
    label_s1 = "Tensión-RMS(V)";
    label_s2 = "Corriente-RMS(A)"
			$.plot("#flot-hist_vi", [
				{ data: data_s1, label: label_s1 , color: "red" },
				{ data: data_s2, label: label_s2, color:"blue" ,yaxis: 2 }
			], options);
		}
		doPlot();
		
		
		var updateInterval = 100;
    function update() {
        singleUpdate(); 
        doPlot();       
        setTimeout(update, updateInterval);
    }
 
    update();
		
		
    
});

function singleUpdate() {
  //console.log("click"); 
  data_s1 = [];
  data_s2 = [];
  switch (parseInt($('#phase_source').val())) {
    case 0:
        $('h3.panel-title').replaceWith("<h3 class='panel-title'> Histograma de Tensión y Corriente Fase A </h3>");
        data_s1 = data_R1;
        data_s2 = data_R2;
        break; 
    case 1:
        $('h3.panel-title').replaceWith("<h3 class='panel-title'> Histograma de Tensión y Corriente Fase B </h3>");
        data_s1 = data_S1;
        data_s2 = data_S2;
        break;
    case 2:
        $('h3.panel-title').replaceWith("<h3 class='panel-title'> Histograma de Tensión y Corriente Fase C </h3>");
        data_s1 = data_T1;
        data_s2 = data_T2;
        break; 
    default: 
        console.log("default")
  }
}


function addData_R(data_fs) {
  data_R1.push([date_fs,data_fs.VR])
  data_R2.push([date_fs,data_fs.IR])       
}

function addData_S(data_fs) {
  data_S1.push([date_fs,data_fs.VS])
  data_S2.push([date_fs,data_fs.IS])       
}

function addData_T(data_fs) {
  data_T1.push([date_fs,data_fs.VT])
  data_T2.push([date_fs,data_fs.IT])       
}

function updateTables(){

    $('td.VR').replaceWith("<td class='VR'>"+data_fs.VR+"</td>");
    $('td.IR').replaceWith("<td class='IR'>"+data_fs.IR+"</td>");
    $('td.VS').replaceWith("<td class='VS'>"+data_fs.VS+"</td>");
    $('td.IS').replaceWith("<td class='IS'>"+data_fs.IS+"</td>");
    $('td.VT').replaceWith("<td class='VT'>"+data_fs.VT+"</td>");
    $('td.IT').replaceWith("<td class='IT'>"+data_fs.IT+"</td>");
    $('td.FPA').replaceWith("<td class='FPA'>"+data_fs.FPA+"</td>");
    $('td.FPB').replaceWith("<td class='FPB'>"+data_fs.FPB+"</td>");
    $('td.FPC').replaceWith("<td class='FPC'>"+data_fs.FPC+"</td>");

}




