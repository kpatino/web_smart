var socket = io.connect('http://192.168.50.1:3700');
var data_fs;
var fpAngular = []


var label_s1;
var label_s2; 

socket.on('connect', function(){
    console.log('we are conencted');
});

socket.on('reconnecting', function(){
 console.log('we are reconnecting');
});

socket.on('disconnect', function(){
    console.log('we are disconected');
});

socket.on('data', function(data){
      data_fs = JSON.parse(data.data);
      updateTables();
      fpCalculator();
      console.log(fpAngular);
});


// Flot Charts sample data for SB Admin template

// Flot Line Chart with Tooltips
$(document).ready(function() {
    console.log("document ready");
    //    socket.emit('page_render',{data : '2'});
    window.chart = new Highcharts.Chart({
        
        chart: {
            renderTo: 'flot-phasorial',
            polar: true
        },
        
        title: {
            text: null
        },
        
        pane: {
            startAngle: 90,
            endAngle: -270
        },
    
        xAxis: {
            tickInterval: 45,
            min: 0,
            max: 360,
            labels: {
               }
        },
            
        yAxis: {
            min: 0
        },
        
        plotOptions: {
            series: {
                pointStart: 0,
                pointInterval: 45
            },
            column: {
                pointPadding: 0,
                groupPadding: 0
            }
        },
    
        series: [ {
            type: 'line',
            name: 'VA',
            data: [[0],[0,1]],
            color : "red"
        },{
            type: 'line',
            name: 'IA',
            data: [[0],[fpAngular[0],1]],
            color : "blue"
        },{
            type: 'line',
            name: 'VB',
            data: [[0],[120,1]],
            color : "red"
        },{
            type: 'line',
            name: 'IB',
            data: [[0],[fpAngular[1],1]],
            color : "blue"
        },{
            type: 'line',
            name: 'VC',
            data: [[0],[240,1]],
            color : "red"
        },{
            type: 'line',
            name: 'IC',
            data: [[0],[fpAngular[2],1]],
            color : "blue"
        }]
    });
    
    
    var updateInterval = 1000;
    function update() {
        //chart.series[0].setData([[0],[90,1]])
        chart.series[1].setData([[0],[fpAngular[0],1]])
        //chart.series[2].setData([[0],[90,1]])
        chart.series[3].setData([[0],[fpAngular[1],1]])
        //chart.series[4].setData([[0],[90,1]])
        chart.series[5].setData([[0],[fpAngular[2],1]])      
        setTimeout(update, updateInterval);
    }
 
    update();
    
		
		
    
});


function updateTables(){

    $('td.VR').replaceWith("<td class='VR'>"+data_fs.VR+"</td>");
    $('td.IR').replaceWith("<td class='IR'>"+data_fs.IR+"</td>");
    $('td.VS').replaceWith("<td class='VS'>"+data_fs.VS+"</td>");
    $('td.IS').replaceWith("<td class='IS'>"+data_fs.IS+"</td>");
    $('td.VT').replaceWith("<td class='VT'>"+data_fs.VT+"</td>");
    $('td.IT').replaceWith("<td class='IT'>"+data_fs.IT+"</td>");
    $('td.FPA').replaceWith("<td class='FPA'>"+data_fs.FPA+"</td>");
    $('td.FPB').replaceWith("<td class='FPB'>"+data_fs.FPB+"</td>");
    $('td.FPC').replaceWith("<td class='FPC'>"+data_fs.FPC+"</td>");

}

function fpCalculator(){

fpAngular[0] = ((Math.acos(data_fs.FPA))*180)/(Math.PI); 
fpAngular[1] = ((Math.acos(data_fs.FPB))*180)/(Math.PI)+120;
fpAngular[2] = ((Math.acos(data_fs.FPC))*180)/(Math.PI)+240;  
}






