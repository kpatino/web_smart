  
var socket = io.connect('http://192.168.50.1:3700');
var data_fs;

var data_VR = [];
var data_IR = [];
var data_VS=  [];
var data_IS = [];
var data_VT = [];
var data_IT = [];
var data_s =  data_VR;

var data_tmp;
var label_s1;
var label_s2; 

socket.on('connect', function(){
    console.log('we are conencted');
});

socket.on('reconnecting', function(){
 console.log('we are reconnecting');
});

socket.on('disconnect', function(){
    console.log('we are disconected');
});

socket.on('data', function(data){
      data_tmp = JSON.parse(data.harm);
      data_VR = data_tmp.VR[0].data;
      data_IR = data_tmp.IR[0].data;
      data_VS = data_tmp.VS[0].data;
      data_IS = data_tmp.IS[0].data;
      data_VT = data_tmp.VT[0].data;
      data_IT = data_tmp.IT[0].data;
      console.log(data_tmp);
      updateTables(data_tmp);

});


 

// Flot Charts sample data for SB Admin template

// Flot Line Chart with Tooltips
$(document).ready(function() {
    console.log("document ready");
    
    //    socket.emit('page_render',{data : '2'});
    /*function currentFormatter(v, axis) {
        return v.toFixed(axis.tickDecimals) + " A";
    }
    
    function voltageFormatter(v, axis) {
        return v.toFixed(axis.tickDecimals) + " V";
    }
    */
    var options = {
        bars: {
                show: true,
                barWidth: 0.3
            }
      ,
      grid: {
          hoverable: true //IMPORTANT! this is needed for tooltip to work
      },
      tooltip: true,
      tooltipOpts: {
          content: "[%x ,%y]",
          shifts: {
          x: -60,
          y: 25
           }
      },
      xaxes: [ { } ],
		  yaxes: [ { min: 0,
		  alignTicksWithAxis: null,
					position: "left"
		              }, {
					// align if we are to the right
					alignTicksWithAxis: null,
					position: "left"
				} ],
				legend: { position: "sw" }   
    };
    
   
    
    function doPlot() {
    label_s1 = "Tensión-RMS(V)";
    label_s2 = "Corriente-RMS(A)"
			$.plot("#flot-hist_vi", [
				{ data: data_s, label: label_s1 , color: "red" },
			], options);
		}
		doPlot();
		
		
		var updateInterval = 100;
    function update() {
        singleUpdate();
        doPlot();       
        setTimeout(update, updateInterval);
    }
 
    update();
		
		
    
});

function singleUpdate() {
  //console.log("click"); 
  //data_s = [];
  switch (parseInt($('#phase_source').val())) { 
    case 0:
        if(parseInt($('#harm_mode').val())==0){
          $('h3.panel-title').replaceWith("<h3 class='panel-title'> Armonicos de Tensión Fase A </h3>");
          data_s = data_VR;
        }else{
          $('h3.panel-title').replaceWith("<h3 class='panel-title'> Armónicos  de Corriente Fase A </h3>");
          data_s = data_IR;
        }    
        break; 
    case 1:
        if(parseInt($('#harm_mode').val())==0){
          $('h3.panel-title').replaceWith("<h3 class='panel-title'> Armónicos de Tensión Fase B </h3>");
          data_s = data_VS;
        }else{
          $('h3.panel-title').replaceWith("<h3 class='panel-title'> Armónicos de Corriente Fase B </h3>");
          data_s = data_IS;
        }    
        break;
    case 2:
        if(parseInt($('#harm_mode').val())==0){
          $('h3.panel-title').replaceWith("<h3 class='panel-title'> Armónicos de Tensión Fase C </h3>");
          data_s = data_VT;
        }else{
          $('h3.panel-title').replaceWith("<h3 class='panel-title'> Armónicos de Corriente Fase C </h3>");
          data_s = data_IT;
        }    
        break; 
    default: 
        console.log("default")
  }
}


function updateTables(data){

    $('td.THD_VA').replaceWith("<td class='THD_VA'>"+data_tmp.VR[0].THD+"</td>");
    $('td.THD_VB').replaceWith("<td class='THD_VB'>"+data_tmp.VS[0].THD+"</td>");
    $('td.THD_VC').replaceWith("<td class='THD_VC'>"+data_tmp.VT[0].THD+"</td>");
    $('td.THD_IA').replaceWith("<td class='THD_IA'>"+data_tmp.IR[0].THD+"</td>");
    $('td.THD_IB').replaceWith("<td class='THD_IB'>"+data_tmp.IS[0].THD+"</td>");
    $('td.THD_IC').replaceWith("<td class='THD_IC'>"+data_tmp.IT[0].THD+"</td>");
   

}




